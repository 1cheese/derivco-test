import * as React from 'react';
import * as ReactDOM from 'react-dom';

import './index.pcss';
import { App } from './CorePackage/MainModule/components/App';

const container = document.querySelector('#app');

ReactDOM.render(
    React.createElement(React.StrictMode, null, React.createElement(App, null)),
    container,
);
