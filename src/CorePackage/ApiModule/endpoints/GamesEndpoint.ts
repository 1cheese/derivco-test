import qs from 'qs';

import { HttpMethod } from '../enums/HttpMethod';
import { MimeType } from '../enums/MimeType';
import { HttpCredentials } from '../enums/HttpCredentials';
import { ErrorHandler } from '../common/ErrorHandler';
import { ResponseHandler } from '../common/ResponseHandler';
import { endpoint } from '../common/endpoint';
import { IGame } from '../../../GamesPackage/MainModule/interfaces/IGame';

export class GamesEndpoint {
    public static getGamesList(): Promise<IGame[]> {
        return fetch(endpoint`/api/games`, {
            method: HttpMethod.GET,
            credentials: HttpCredentials.SameOrigin,
            headers: {
                Accept: MimeType.APPLICATION_JSON,
            },
        })
            .then(ErrorHandler.commonHandler)
            .then(ResponseHandler.commonHandler)
            .catch(error => console.error(error));
    }

    public static createGame(formData: IGame): Promise<IGame> {
        const serializedFormData = qs.stringify(formData, {
            encode: false,
        });
        const requestBody = new URLSearchParams(serializedFormData);

        return fetch(endpoint`/api/games`, {
            method: HttpMethod.POST,
            body: requestBody,
            credentials: HttpCredentials.SameOrigin,
            headers: {
                Accept: MimeType.APPLICATION_JSON,
            },
        })
            .then(ErrorHandler.commonHandler)
            .then(ResponseHandler.commonHandler)
            .catch(error => console.error(error));
    }

    public static editGame(formData: IGame): Promise<IGame> {
        const serializedFormData = qs.stringify(formData, {
            encode: false,
        });
        const requestBody = new URLSearchParams(serializedFormData);

        return fetch(endpoint`/api/games/${formData.id}`, {
            method: HttpMethod.PUT,
            body: requestBody,
            credentials: HttpCredentials.SameOrigin,
            headers: {
                Accept: MimeType.APPLICATION_JSON,
            },
        })
            .then(ErrorHandler.commonHandler)
            .then(ResponseHandler.commonHandler)
            .catch(error => console.error(error));
    }

    public static getGameById(gameId: IGame['id']): Promise<IGame> {
        return fetch(endpoint`/api/games/${gameId}`, {
            method: HttpMethod.GET,
            credentials: HttpCredentials.SameOrigin,
            headers: {
                Accept: MimeType.APPLICATION_JSON,
            },
        })
            .then(ErrorHandler.commonHandler)
            .then(ResponseHandler.commonHandler)
            .catch(error => console.error(error));
    }

    public static deleteGameById(gameId: IGame['id']): Promise<void> {
        return fetch(endpoint`/api/games/${gameId}`, {
            method: HttpMethod.DELETE,
            credentials: HttpCredentials.SameOrigin,
            headers: {
                Accept: MimeType.APPLICATION_JSON,
            },
        })
            .then(ErrorHandler.commonHandler)
            .then(ResponseHandler.commonHandler)
            .catch(error => console.error(error));
    }
}
