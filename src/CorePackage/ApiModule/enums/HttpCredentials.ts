export enum HttpCredentials {
    Omit = 'omit',
    SameOrigin = 'same-origin',
    Include = 'include',
}
