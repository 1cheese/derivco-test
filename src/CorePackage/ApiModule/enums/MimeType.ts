export enum MimeType {
    ANY = '*/*',

    TEXT_PLAIN = 'text/plain',
    TEXT_HTML = 'text/html',

    IMAGE_JPEG = 'image/jpeg',
    IMAGE_PNG = 'image/png',

    AUDIO = 'audio/*',
    AUDIO_MPEG = 'audio/mpeg',
    AUDIO_OGG = 'audio/ogg',

    VIDEO_MP4 = 'video/mp4',

    APPLICATION = 'application/*',
    APPLICATION_JSON = 'application/json',
    APPLICATION_JAVASCRIPT = 'application/javascript',
    APPLICATION_ECMASCRIPT = 'application/ecmascript',
    APPLICATION_OCTET_STREAM = 'application/octet-stream',
    APPLICATION_EXCEL = 'application/vnd.ms-excel',
}
