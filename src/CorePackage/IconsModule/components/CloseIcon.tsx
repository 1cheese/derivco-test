import * as React from 'react';

export const CloseIcon: React.FC = () => (
    <svg
        width="14"
        height="14"
        viewBox="0 0 14 14"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
    >
        <path
            d="M0.874475 0.874475L13.329 13.329"
            stroke="currentColor"
            strokeLinecap="round"
        />
        <path
            d="M13.1255 0.874475L0.670959 13.329"
            stroke="currentColor"
            strokeLinecap="round"
        />
    </svg>
);
