import * as React from 'react';

import css from './Modal.pcss';
import { CloseIcon } from '../../IconsModule/components/CloseIcon';

type ModalProps = {
    isOpened: boolean;
    title: string;
    buttons?: React.ReactNode[];
    className?: string;
    onClose: () => void;
};

const Modal: React.FC<ModalProps> = ({
    isOpened,
    buttons,
    title,
    className,
    children,
    onClose,
}) => {
    const closeModalOnEsc = (event: KeyboardEvent): void => {
        if (event.code === 'Escape') {
            onClose();
        }
    };

    React.useEffect(() => {
        document.addEventListener('keyup', closeModalOnEsc);

        return () => {
            document.removeEventListener('keyup', closeModalOnEsc);
        };
    });

    return isOpened === false ? null : (
        <div className={css.modal}>
            <div className={css.background} onClick={onClose} />
            <div className={`${css.main} ${className ?? ''}`}>
                <div className={css.header}>
                    <h2 className={css.headerTitle}>{title}</h2>
                    <span className={css.closeIcon} onClick={onClose}>
                        <CloseIcon />
                    </span>
                </div>

                <div className={css.content}>{children}</div>

                {buttons !== undefined && (
                    <div className={css.buttons}>
                        {buttons.map((button, index) => (
                            <React.Fragment key={index}>
                                {button}
                            </React.Fragment>
                        ))}
                    </div>
                )}
            </div>
        </div>
    );
};

export { Modal, ModalProps };
