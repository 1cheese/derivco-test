import * as React from 'react';
import ReactDOM from 'react-dom';

type PopupPortalProps = {};

const popupRoot = document.getElementById('popup-root');

export class PopupPortal extends React.PureComponent<PopupPortalProps> {
    private readonly el: HTMLDivElement;

    constructor(props: PopupPortalProps) {
        super(props);

        this.el = document.createElement('div');
    }

    public componentDidMount(): void {
        popupRoot?.appendChild(this.el);
    }

    public componentWillUnmount(): void {
        popupRoot?.removeChild(this.el);
    }

    render() {
        return ReactDOM.createPortal(this.props.children, this.el);
    }
}
