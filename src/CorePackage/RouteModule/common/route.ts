import { IRouteStatic } from '../interfaces';
import { RouteParams } from '../types/RouteParams';

/* eslint-disable @typescript-eslint/no-empty-function */
export function route<P extends RouteParams = {}>() {
    return (constructor: IRouteStatic<P>) => {};
}
/* eslint-enable @typescript-eslint/no-empty-function */
