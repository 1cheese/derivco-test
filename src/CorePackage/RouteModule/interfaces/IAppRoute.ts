import { RouteComponentProps } from 'react-router-dom';

export interface IAppRoute<P> {
    props: RouteComponentProps<P>;
}
