import { IAppRoute } from './IAppRoute';

export interface IRouteStatic<P> {
    new (...args: unknown[]): IAppRoute<P>;

    url(params: P): string;
}
