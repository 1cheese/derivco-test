import * as React from 'react';
import MaterialTextField, {
    Input,
    Props as TextFieldProps,
} from '@material/react-text-field';
import '@material/react-text-field/dist/text-field.css';

import css from './TextField.pcss';
import { classList } from '../../MainModule/utils/classList';

type Props = Omit<TextFieldProps<HTMLInputElement>, 'children'> &
    Input['props'];

const TextField: React.FunctionComponent<Props> = ({
    className,
    label,
    dense = true,
    outlined = true,
    textarea,
    value,
    onChange,
}) => (
    <MaterialTextField
        label={label}
        dense={dense}
        outlined={outlined}
        className={classList({
            [css.textField]: true,
            [className as string]: className !== undefined,
        })}
        textarea={textarea}
        floatingLabelClassName={css.floatingLabel}
    >
        <Input value={value} className={css.input} onChange={onChange} />
    </MaterialTextField>
);

export { TextField };
