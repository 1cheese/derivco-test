import * as React from 'react';
import MaterialButton, { ButtonProps } from '@material/react-button';
import '@material/react-button/dist/button.css';

import css from './Button.pcss';
import { classList } from '../../MainModule/utils/classList';
import { ButtonType } from '../enums/ButtonType';

type Props = ButtonProps<HTMLAnchorElement | HTMLButtonElement> & {
    buttonType?: ButtonType;
};

export const Button: React.FC<Props> = ({
    buttonType,
    children,
    className,
    dense = false,
    disabled,
    raised = true,
    title,
    onClick,
}) => (
    <MaterialButton
        className={classList({
            [className as string]: className !== undefined,
            [css.button]: true,
            [css.buttonWarning]: buttonType === ButtonType.Warning,
            [css.buttonSuccess]: buttonType === ButtonType.Success,
        })}
        dense={dense}
        disabled={disabled}
        raised={raised}
        title={title}
        onClick={onClick}
    >
        {children}
    </MaterialButton>
);
