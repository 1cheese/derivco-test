import * as React from 'react';

import css from './PageTitle.pcss';

type PageTitleProps = {
    label: string | React.ReactNode;
    buttons: React.ReactNode[];
};

const PageTitle: React.FC<PageTitleProps> = ({ label, buttons }) => (
    <div className={css.component}>
        <div className={css.label}>{label}</div>
        {buttons.length && (
            <div className={css.buttons}>
                {buttons.map((button, index) => (
                    <React.Fragment key={index}>{button}</React.Fragment>
                ))}
            </div>
        )}
    </div>
);

export { PageTitle, PageTitleProps };
