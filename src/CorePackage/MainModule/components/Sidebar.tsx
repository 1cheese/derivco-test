import * as React from 'react';
import { Link } from 'react-router-dom';

import css from './Sidebar.pcss';
import { DerivcoLogo } from '../../IconsModule/components/DerivcoLogo';
import { IndexRoute } from '../routes/IndexRoute';

function renderGroupOfItems(numberOfItems: number): React.ReactNode {
    return (
        <div className={css.group}>
            {new Array(numberOfItems).fill('').map((element, index) => (
                <div className={css.item} key={index} />
            ))}
        </div>
    );
}

const Sidebar: React.FC = () => (
    <div className={css.component}>
        <div className={css.logo}>
            <Link to={IndexRoute.url()}>
                <DerivcoLogo />
            </Link>
        </div>
        {renderGroupOfItems(3)}
        {renderGroupOfItems(2)}
        {renderGroupOfItems(1)}
    </div>
);

export { Sidebar };
