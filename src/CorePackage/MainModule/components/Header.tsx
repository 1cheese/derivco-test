import * as React from 'react';

import css from './Header.pcss';

const Header: React.FC = () => (
    <div className={css.component}>
        <div className={css.group}>
            {new Array(5).fill('').map((element, index) => (
                <div className={css.item} key={index} />
            ))}
        </div>
    </div>
);

export { Header };
