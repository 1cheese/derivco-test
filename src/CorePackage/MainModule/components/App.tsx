import * as React from 'react';
import { Provider } from 'react-redux';
import { Router } from 'react-router-dom';

import { RootRouter } from '../containers/RootRouter';
import { configureStore } from '../utils/configureStore';
import { browserHistory } from '../utils/browserHistory';

const App: React.FC = () => {
    const store = configureStore();

    return (
        <Provider store={store}>
            <Router history={browserHistory}>
                <RootRouter />
            </Router>
        </Provider>
    );
};

export { App };
