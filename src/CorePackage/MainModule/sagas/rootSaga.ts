import { all, fork } from 'redux-saga/effects';

import * as gamesListSaga from '../../../GamesPackage/GamesListModule/sagas';
import * as fetchGameByIdSaga from '../../../GamesPackage/CreateAndEditModule/sagas';

export function* rootSaga() {
    yield all(
        Object.values(gamesListSaga).map(currentSaga => fork(currentSaga)),
    );
    yield all(
        Object.values(fetchGameByIdSaga).map(currentSaga => fork(currentSaga)),
    );
}
