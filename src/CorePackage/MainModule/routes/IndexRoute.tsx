import { route } from '../../RouteModule/common/route';
import { AbstractRoute } from '../../RouteModule/common/AbstractRoute';

@route()
export class IndexRoute extends AbstractRoute {
    public static url() {
        return '/';
    }
}
