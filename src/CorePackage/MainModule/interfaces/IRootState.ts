import { IGamesListState } from '../../../GamesPackage/GamesListModule/interfaces/IGamesListState';
import { ICreateAndEditGameState } from '../../../GamesPackage/CreateAndEditModule/interfaces/ICreateAndEditGameState';

export interface IRootState {
    GamesList: IGamesListState;
    CreateAndEditGame: ICreateAndEditGameState;
}
