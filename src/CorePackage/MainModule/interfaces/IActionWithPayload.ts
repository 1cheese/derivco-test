import { Action } from 'redux';

export interface IActionWithPayload<T, U> extends Action<T> {
    payload: U;
}
