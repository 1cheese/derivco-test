import { applyMiddleware, compose, createStore } from 'redux';
import createSagaMiddleware from 'redux-saga';

import { rootSaga } from '../sagas/rootSaga';
import { rootReducer } from '../reducers/rootReducer';

export function configureStore() {
    const sagaMiddleware = createSagaMiddleware();

    const extendedCompose = (window as any)
        .__REDUX_DEVTOOLS_EXTENSION_COMPOSE__;
    const composeEnhancers =
        typeof extendedCompose === 'undefined' ? compose : extendedCompose;

    const store = createStore(
        rootReducer,
        composeEnhancers(applyMiddleware(sagaMiddleware)),
    );

    sagaMiddleware.run(rootSaga);

    return store;
}
