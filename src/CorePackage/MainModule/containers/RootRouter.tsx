import * as React from 'react';
import { Switch } from 'react-router-dom';

import { MainLayout } from '../components/MainLayout';
import { IndexRouter } from '../router/IndexRouter';
import { GamesListRouter } from '../../../GamesPackage/GamesListModule/routers/GamesListRouter';
import { CreateGameRouter } from '../../../GamesPackage/CreateAndEditModule/routers/CreateGameRouter';
import { EditGameRouter } from '../../../GamesPackage/CreateAndEditModule/routers/EditGameRouter';

const RootRouter: React.FC = () => {
    return (
        <MainLayout>
            <Switch>
                <IndexRouter />
                <GamesListRouter />
                <CreateGameRouter />
                <EditGameRouter />
            </Switch>
        </MainLayout>
    );
};

export { RootRouter };
