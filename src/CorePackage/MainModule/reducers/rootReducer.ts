import { combineReducers, Reducer } from 'redux';

import { IRootState } from '../interfaces/IRootState';
import { gamesListReducer } from '../../../GamesPackage/GamesListModule/reducers';
import { createAndEditGameReducer } from '../../../GamesPackage/CreateAndEditModule/reducers';

export const rootReducer: Reducer<IRootState> = combineReducers({
    GamesList: gamesListReducer,
    CreateAndEditGame: createAndEditGameReducer,
});
