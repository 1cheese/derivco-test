import * as React from 'react';
import { Route, RouteProps } from 'react-router-dom';

import { GamesListRoute } from '../routes/GamesListRoute';
import { GamesListPage } from '../components/GamesListPage';

type Props = RouteProps & {};

export class GamesListRouter extends React.Component<Props> {
    public static readonly defaultProps: Partial<Props> = {
        exact: true,
        path: GamesListRoute.url(),
    };

    public render() {
        return <Route {...this.props} component={GamesListPage} />;
    }
}
