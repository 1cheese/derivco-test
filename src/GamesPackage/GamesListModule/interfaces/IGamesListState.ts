import { IGame } from '../../MainModule/interfaces/IGame';

export interface IGamesListState {
    isFetching: boolean;
    games: IGame[];
}
