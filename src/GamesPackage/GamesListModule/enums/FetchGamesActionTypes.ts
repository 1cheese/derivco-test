export enum FetchGamesActionTypes {
    Initial = 'FETCH_GAMES',
    Pending = 'FETCH_GAMES_PENDING',
    Rejected = 'FETCH_GAMES_REJECTED',
    Fulfilled = 'FETCH_GAMES_FULFILLED',
}
