export enum InsertPosition {
    Before = -1,
    After = 1,
}
