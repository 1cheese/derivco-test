export enum DeleteGameActionTypes {
    Initial = 'DELETE_GAME',
    Pending = 'DELETE_GAME_PENDING',
    Rejected = 'DELETE_GAME_REJECTED',
    Fulfilled = 'DELETE_GAME_FULFILLED',
}
