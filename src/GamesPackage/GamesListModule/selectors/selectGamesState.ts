import { createSelector } from 'reselect';

import { IRootState } from '../../../CorePackage/MainModule/interfaces/IRootState';

export const selectGamesState = createSelector(
    (state: IRootState) => state,
    state => state.GamesList,
);
