import { createSelector } from 'reselect';

import { selectGamesState } from './selectGamesState';

export const selectGamesIsFetching = createSelector(
    selectGamesState,
    gamesState => gamesState.isFetching,
);
