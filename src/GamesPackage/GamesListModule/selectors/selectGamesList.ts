import { createSelector } from 'reselect';

import { selectGamesState } from './selectGamesState';

export const selectGamesList = createSelector(
    selectGamesState,
    gamesState => gamesState.games,
);
