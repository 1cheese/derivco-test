import { createSelector } from 'reselect';

import { selectGamesList } from './selectGamesList';
import { IGame } from '../../MainModule/interfaces/IGame';

export const selectGameById = (gameId: IGame['id']) =>
    createSelector(selectGamesList, gamesList =>
        gamesList.find(({ id }) => id === gameId),
    );
