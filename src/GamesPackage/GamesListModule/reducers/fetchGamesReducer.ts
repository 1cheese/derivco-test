import { Reducer } from 'redux';

import { IGamesListState } from '../interfaces/IGamesListState';
import { gamesListDefaultState } from '../utils/gamesListDefaultState';
import { FetchGamesActionTypes } from '../enums/FetchGamesActionTypes';
import { IActionWithPayload } from '../../../CorePackage/MainModule/interfaces/IActionWithPayload';
import { IGame } from '../../MainModule/interfaces/IGame';

type TAction = IActionWithPayload<FetchGamesActionTypes, IGame[]>;

export const fetchGamesReducer: Reducer<IGamesListState, TAction> = (
    state = gamesListDefaultState,
    action,
): IGamesListState => {
    switch (action.type) {
        case FetchGamesActionTypes.Pending:
            return {
                ...state,
                isFetching: true,
            };

        case FetchGamesActionTypes.Rejected:
            return {
                ...state,
                isFetching: false,
            };

        case FetchGamesActionTypes.Fulfilled:
            return {
                ...state,
                isFetching: false,
                games: action.payload,
            };

        default:
            return state;
    }
};
