import { Action, Reducer } from 'redux';

import { IGamesListState } from '../interfaces/IGamesListState';
import { gamesListDefaultState } from '../utils/gamesListDefaultState';
import { DeleteGameActionTypes } from '../enums/DeleteGameActionTypes';

type TAction = Action<DeleteGameActionTypes>;

export const deleteGameReducer: Reducer<IGamesListState, TAction> = (
    state = gamesListDefaultState,
    action,
): IGamesListState => {
    switch (action.type) {
        case DeleteGameActionTypes.Pending:
            return {
                ...state,
                isFetching: true,
            };

        case DeleteGameActionTypes.Rejected:
            return {
                ...state,
                isFetching: false,
            };

        case DeleteGameActionTypes.Fulfilled:
            return {
                ...state,
                isFetching: false,
            };

        default:
            return state;
    }
};
