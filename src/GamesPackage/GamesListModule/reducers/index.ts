import reduceReducers from 'reduce-reducers';

import { gamesListDefaultState } from '../utils/gamesListDefaultState';
import { fetchGamesReducer } from './fetchGamesReducer';
import { IGamesListState } from '../interfaces/IGamesListState';
import { deleteGameReducer } from './deleteGameReducer';

export const gamesListReducer = reduceReducers<IGamesListState, any>(
    gamesListDefaultState ?? null,
    fetchGamesReducer,
    deleteGameReducer,
);
