import { call, put, takeLatest } from 'redux-saga/effects';

import {
    deleteGameFulfilledAction,
    deleteGamePendingAction,
    deleteGameRejectedAction,
} from '../actions/DeleteGameAction';
import { GamesEndpoint } from '../../../CorePackage/ApiModule/endpoints/GamesEndpoint';
import { IGame } from '../../MainModule/interfaces/IGame';
import { DeleteGameActionTypes } from '../enums/DeleteGameActionTypes';
import { fetchGamesSaga } from './fetchGamesSaga';
import { IActionWithPayload } from '../../../CorePackage/MainModule/interfaces/IActionWithPayload';

export function* deleteGameSaga(
    action: IActionWithPayload<DeleteGameActionTypes.Initial, IGame['id']>,
) {
    try {
        yield put(deleteGamePendingAction());
        yield call(GamesEndpoint.deleteGameById, action.payload);
        yield put(deleteGameFulfilledAction());
        yield call(fetchGamesSaga);
    } catch (error) {
        yield put(deleteGameRejectedAction());
    }
}

export function* watchDeleteGameSaga() {
    yield takeLatest(DeleteGameActionTypes.Initial, deleteGameSaga);
}
