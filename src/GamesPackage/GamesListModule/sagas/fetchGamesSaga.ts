import { call, put, takeLatest } from 'redux-saga/effects';

import {
    fetchGamesFulfilledAction,
    fetchGamesPendingAction,
    fetchGamesRejectedAction,
} from '../actions/FetchGamesAction';
import { GamesEndpoint } from '../../../CorePackage/ApiModule/endpoints/GamesEndpoint';
import { IGame } from '../../MainModule/interfaces/IGame';
import { FetchGamesActionTypes } from '../enums/FetchGamesActionTypes';

export function* fetchGamesSaga() {
    try {
        yield put(fetchGamesPendingAction());
        const data: IGame[] = yield call(GamesEndpoint.getGamesList);
        yield put(fetchGamesFulfilledAction(data));
    } catch (error) {
        yield put(fetchGamesRejectedAction());
    }
}

export function* watchFetchGamesSaga() {
    yield takeLatest(FetchGamesActionTypes.Initial, fetchGamesSaga);
}
