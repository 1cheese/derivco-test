import { Action } from 'redux';

import { DeleteGameActionTypes } from '../enums/DeleteGameActionTypes';
import { IGame } from '../../MainModule/interfaces/IGame';
import { IActionWithPayload } from '../../../CorePackage/MainModule/interfaces/IActionWithPayload';

export function deleteGameAction(
    payload: IGame['id'],
): IActionWithPayload<DeleteGameActionTypes.Initial, IGame['id']> {
    return {
        type: DeleteGameActionTypes.Initial,
        payload: payload,
    };
}

export function deleteGamePendingAction(): Action {
    return {
        type: DeleteGameActionTypes.Pending,
    };
}

export function deleteGameRejectedAction(): Action {
    return {
        type: DeleteGameActionTypes.Rejected,
    };
}

export function deleteGameFulfilledAction(): Action {
    return {
        type: DeleteGameActionTypes.Fulfilled,
    };
}
