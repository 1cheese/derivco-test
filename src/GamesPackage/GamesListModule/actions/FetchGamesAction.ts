import { Action } from 'redux';

import { IGame } from '../../MainModule/interfaces/IGame';
import { FetchGamesActionTypes } from '../enums/FetchGamesActionTypes';
import { IActionWithPayload } from '../../../CorePackage/MainModule/interfaces/IActionWithPayload';

export function fetchGamesAction(): Action {
    return {
        type: FetchGamesActionTypes.Initial,
    };
}

export function fetchGamesPendingAction(): Action {
    return {
        type: FetchGamesActionTypes.Pending,
    };
}

export function fetchGamesRejectedAction(): Action {
    return {
        type: FetchGamesActionTypes.Rejected,
    };
}

export function fetchGamesFulfilledAction(
    payload: IGame[],
): IActionWithPayload<FetchGamesActionTypes.Fulfilled, IGame[]> {
    return {
        type: FetchGamesActionTypes.Fulfilled,
        payload: payload,
    };
}
