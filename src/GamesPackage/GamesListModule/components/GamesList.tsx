import * as React from 'react';
import {
    closestCenter,
    DndContext,
    DragEndEvent,
    DragOverlay,
    DragStartEvent,
    PointerSensor,
    useSensor,
    useSensors,
} from '@dnd-kit/core';
import { arrayMove, SortableContext } from '@dnd-kit/sortable';

import css from './GamesList.pcss';
import { GameCard } from './GameCard';
import { IGame } from '../../MainModule/interfaces/IGame';
import { SortableGameCard } from './SortableGameCard';

interface GamesListProps {
    games: IGame[];
}

const GamesList: React.FC<GamesListProps> = ({ games }) => {
    const [activeId, setActiveId] = React.useState<IGame['id'] | null>(null);
    const gameIds = games.map(({ id }) => id);
    const [ids, setIds] = React.useState(gameIds);

    React.useEffect(() => {
        const gameIds = games.map(({ id }) => id);
        setIds(gameIds);
    }, [games]);

    const activeIndex = activeId ? ids.indexOf(activeId) : -1;
    const sensors = useSensors(useSensor(PointerSensor));

    const handleDragStart = ({ active }: DragStartEvent) => {
        setActiveId(active.id);
    };

    const handleDragCancel = () => {
        setActiveId(null);
    };

    const handleDragEnd = ({ over }: DragEndEvent) => {
        if (over) {
            const overIndex = ids.indexOf(over.id);

            if (activeIndex !== overIndex) {
                const newIndex = overIndex;

                setIds(items => arrayMove(items, activeIndex, newIndex));
            }
        }

        setActiveId(null);
    };

    return (
        <DndContext
            onDragStart={handleDragStart}
            onDragEnd={handleDragEnd}
            onDragCancel={handleDragCancel}
            sensors={sensors}
            collisionDetection={closestCenter}
        >
            <SortableContext items={ids}>
                <div className={css.gamesList}>
                    {ids.map((id, index) => (
                        <SortableGameCard
                            id={id}
                            index={index}
                            key={id}
                            activeIndex={activeIndex}
                        />
                    ))}
                </div>
            </SortableContext>
            <DragOverlay>
                {activeId && <GameCard id={activeId} clone />}
            </DragOverlay>
        </DndContext>
    );
};

export { GamesList, GamesListProps };
