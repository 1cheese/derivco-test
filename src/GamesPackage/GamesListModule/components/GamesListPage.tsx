import * as React from 'react';
import { useDispatch, useSelector } from 'react-redux';

import css from './GamesListPage.pcss';
import { selectGamesIsFetching } from '../selectors/selectGamesIsFetching';
import { selectGamesList } from '../selectors/selectGamesList';
import { fetchGamesAction } from '../actions/FetchGamesAction';
import { Button } from '../../../CorePackage/ControlsModule/components/Button';
import { PageTitle } from '../../../CorePackage/MainModule/components/PageTitle';
import { browserHistory } from '../../../CorePackage/MainModule/utils/browserHistory';
import { CreateGameRoute } from '../../CreateAndEditModule/routes/CreateGameRoute';
import { GamesList } from './GamesList';

const GamesListPage: React.FC = () => {
    const dispatch = useDispatch();
    const isFetching = useSelector(selectGamesIsFetching);
    const games = useSelector(selectGamesList);

    React.useEffect(() => {
        dispatch(fetchGamesAction());
    }, []);

    const handleAddGameButtonClick = () => {
        browserHistory.push(CreateGameRoute.url());
    };

    return (
        <>
            <div className={css.component}>
                <PageTitle
                    label="Games list"
                    buttons={[
                        <Button
                            title="Add game"
                            disabled={isFetching}
                            onClick={handleAddGameButtonClick}
                        >
                            Add game
                        </Button>,
                    ]}
                />
                <div className={css.list}>
                    {isFetching ? (
                        <div className={css.preloader}>Loading...</div>
                    ) : (
                        <GamesList games={games} />
                    )}
                </div>
            </div>
        </>
    );
};

export { GamesListPage };
