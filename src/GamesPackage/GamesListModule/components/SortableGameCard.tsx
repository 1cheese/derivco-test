import * as React from 'react';
import { GameCard, GameCardProps } from './GameCard';
import { useSortable } from '@dnd-kit/sortable';
import { CSS } from '@dnd-kit/utilities';

import { InsertPosition } from '../enums/InsertPosition';

const SortableGameCard: React.FC<GameCardProps & { activeIndex: number }> = ({
    id,
    activeIndex,
    ...props
}) => {
    const {
        attributes,
        listeners,
        index,
        isDragging,
        isSorting,
        over,
        setNodeRef,
        transform,
        transition,
    } = useSortable({
        id: id,
        animateLayoutChanges: () => true,
    });

    return (
        <GameCard
            ref={setNodeRef}
            id={id}
            active={isDragging}
            style={{
                transition,
                transform: isSorting
                    ? undefined
                    : CSS.Translate.toString(transform),
            }}
            insertPosition={
                over?.id === id
                    ? index > activeIndex
                        ? InsertPosition.After
                        : InsertPosition.Before
                    : undefined
            }
            {...props}
            {...attributes}
            {...listeners}
        />
    );
};

export { SortableGameCard };
