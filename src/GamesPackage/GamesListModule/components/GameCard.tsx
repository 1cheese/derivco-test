import * as React from 'react';
import { useDispatch, useSelector } from 'react-redux';

import css from './GameCard.pcss';
import { IGame } from '../../MainModule/interfaces/IGame';
import { TrashBinIcon } from '../../../CorePackage/IconsModule/components/TrashBinIcon';
import { PopupPortal } from '../../../CorePackage/PopupModule/components/PopupPortal';
import { Modal } from '../../../CorePackage/PopupModule/components/Modal';
import { Button } from '../../../CorePackage/ControlsModule/components/Button';
import { ButtonType } from '../../../CorePackage/ControlsModule/enums/ButtonType';
import { deleteGameAction } from '../actions/DeleteGameAction';
import { PencilIcon } from '../../../CorePackage/IconsModule/components/PencilIcon';
import { browserHistory } from '../../../CorePackage/MainModule/utils/browserHistory';
import { EditGameRoute } from '../../CreateAndEditModule/routes/EditGameRoute';
import { classList } from '../../../CorePackage/MainModule/utils/classList';
import { InsertPosition } from '../enums/InsertPosition';
import { selectGameById } from '../selectors/selectGameById';
import { IRootState } from '../../../CorePackage/MainModule/interfaces/IRootState';

interface GameCardProps
    extends Omit<React.HTMLAttributes<HTMLDivElement>, 'id'> {
    active?: boolean;
    clone?: boolean;
    insertPosition?: InsertPosition;
    id: IGame['id'];
    index?: number;
}

const GameCard = React.forwardRef<HTMLDivElement, GameCardProps>(
    function GameCard(
        { id, index, active, clone, insertPosition, style, ...props },
        ref,
    ) {
        const game = useSelector<IRootState, IGame | undefined>(state =>
            selectGameById(id)(state),
        );

        const dispatch = useDispatch();
        const [isModalOpened, setIsModalOpened] = React.useState(false);

        const handleEditButtonClick = () => {
            browserHistory.push(EditGameRoute.url({ id: id }));
        };

        const handleDeleteButtonClick = () => {
            setIsModalOpened(true);
        };

        const handleSubmitDeleteButtonClick = () => {
            dispatch(deleteGameAction(id));
        };

        const handleModalClose = () => {
            setIsModalOpened(false);
        };

        return game === undefined ? null : (
            <>
                <div
                    className={classList({
                        [css.wrapper]: true,
                        [css.active]: active === true,
                        [css.clone]: clone === true,
                        [css.insertBefore]:
                            insertPosition === InsertPosition.Before,
                        [css.insertAfter]:
                            insertPosition === InsertPosition.After,
                    })}
                    style={style}
                >
                    <div className={css.card} ref={ref} data-id={id}>
                        <h3 className={css.title} {...props}>
                            {game.name}
                        </h3>
                        <div className={css.desc}>{game.description}</div>
                        <div className={css.controls}>
                            <button
                                className={css.edit}
                                onClick={handleEditButtonClick}
                            >
                                <PencilIcon />
                            </button>
                            <button
                                className={css.remove}
                                onClick={handleDeleteButtonClick}
                            >
                                <TrashBinIcon />
                            </button>
                        </div>
                    </div>
                </div>
                <PopupPortal>
                    <Modal
                        isOpened={isModalOpened}
                        title={`Are you sure you want to delete game "${game.name}"`}
                        onClose={handleModalClose}
                        buttons={[
                            <Button
                                buttonType={ButtonType.Warning}
                                onClick={handleSubmitDeleteButtonClick}
                            >
                                Delete
                            </Button>,
                            <Button onClick={handleModalClose}>Cancel</Button>,
                        ]}
                    />
                </PopupPortal>
            </>
        );
    },
);

export { GameCard, GameCardProps };
