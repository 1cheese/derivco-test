import { IGamesListState } from '../interfaces/IGamesListState';

export const gamesListDefaultState: IGamesListState = {
    isFetching: false,
    games: [],
};
