import { createSelector } from 'reselect';

import { selectCreateAndEditGameState } from './selectCreateAndEditGameState';

export const selectCreateAndEditGameIsFetching = createSelector(
    selectCreateAndEditGameState,
    state => state.isFetching,
);
