import { createSelector } from 'reselect';

import { selectCreateAndEditGameState } from './selectCreateAndEditGameState';

export const selectCreateAndEditGameFields = createSelector(
    selectCreateAndEditGameState,
    state => state.fields,
);
