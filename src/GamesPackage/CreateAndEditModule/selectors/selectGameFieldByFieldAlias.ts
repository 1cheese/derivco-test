import { createSelector } from 'reselect';

import { GameFieldAlias } from '../enums/GameFieldAlias';
import { selectCreateAndEditGameFields } from './selectCreateAndEditGameFields';

export const selectGameFieldByFieldAlias = (fieldAlias: GameFieldAlias) =>
    createSelector(selectCreateAndEditGameFields, fields => fields[fieldAlias]);
