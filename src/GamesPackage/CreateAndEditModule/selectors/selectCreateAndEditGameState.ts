import { createSelector } from 'reselect';

import { IRootState } from '../../../CorePackage/MainModule/interfaces/IRootState';

export const selectCreateAndEditGameState = createSelector(
    (state: IRootState) => state,
    state => state.CreateAndEditGame,
);
