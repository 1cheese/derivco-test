import { IActionWithPayload } from '../../../CorePackage/MainModule/interfaces/IActionWithPayload';
import { SetGameFieldByAliasActionTypes } from '../enums/SetGameFieldByAliasActionTypes';
import { ISetGameFieldByAliasActionPayload } from '../interfaces/ISetGameFieldByAliasPayload';

export function setGameFieldByAliasAction(
    payload: ISetGameFieldByAliasActionPayload,
): IActionWithPayload<
    SetGameFieldByAliasActionTypes.SetField,
    ISetGameFieldByAliasActionPayload
> {
    return {
        type: SetGameFieldByAliasActionTypes.SetField,
        payload: payload,
    };
}
