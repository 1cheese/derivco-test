import { Action } from 'redux';

import { IGame } from '../../MainModule/interfaces/IGame';
import { IActionWithPayload } from '../../../CorePackage/MainModule/interfaces/IActionWithPayload';
import { EditGameActionTypes } from '../enums/EditGameActionTypes';

export function editGameAction(
    payload: IGame['id'],
): IActionWithPayload<EditGameActionTypes.Initial, IGame['id']> {
    return {
        type: EditGameActionTypes.Initial,
        payload: payload,
    };
}

export function editGamePendingAction(): Action {
    return {
        type: EditGameActionTypes.Pending,
    };
}

export function editGameRejectedAction(): Action {
    return {
        type: EditGameActionTypes.Rejected,
    };
}

export function editGameFulfilledAction(
    payload: IGame,
): IActionWithPayload<EditGameActionTypes.Fulfilled, IGame> {
    return {
        type: EditGameActionTypes.Fulfilled,
        payload: payload,
    };
}
