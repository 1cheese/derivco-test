import { Action } from 'redux';

import { IGame } from '../../MainModule/interfaces/IGame';
import { IActionWithPayload } from '../../../CorePackage/MainModule/interfaces/IActionWithPayload';
import { FetchGameByIdActionTypes } from '../enums/FetchGameByIdActionTypes';

export function fetchGameByIdAction(
    payload: IGame['id'],
): IActionWithPayload<FetchGameByIdActionTypes.Initial, IGame['id']> {
    return {
        type: FetchGameByIdActionTypes.Initial,
        payload: payload,
    };
}

export function fetchGameByIdPendingAction(): Action {
    return {
        type: FetchGameByIdActionTypes.Pending,
    };
}

export function fetchGameByIdRejectedAction(): Action {
    return {
        type: FetchGameByIdActionTypes.Rejected,
    };
}

export function fetchGameByIdFulfilledAction(
    payload: IGame,
): IActionWithPayload<FetchGameByIdActionTypes.Fulfilled, IGame> {
    return {
        type: FetchGameByIdActionTypes.Fulfilled,
        payload: payload,
    };
}
