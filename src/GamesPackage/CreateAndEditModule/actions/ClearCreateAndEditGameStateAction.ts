import { Action } from 'redux';

import { ClearCreateAndEditGameStateActionTypes } from '../enums/ClearCreateAndEditGameStateActionTypes';

export function clearCreateAndEditGameStateAction(): Action {
    return {
        type: ClearCreateAndEditGameStateActionTypes.ClearState,
    };
}
