import { Action } from 'redux';

import { IGame } from '../../MainModule/interfaces/IGame';
import { IActionWithPayload } from '../../../CorePackage/MainModule/interfaces/IActionWithPayload';
import { CreateGameActionTypes } from '../enums/CreateGameActionTypes';

export function createGameAction(): Action {
    return {
        type: CreateGameActionTypes.Initial,
    };
}

export function createGamePendingAction(): Action {
    return {
        type: CreateGameActionTypes.Pending,
    };
}

export function createGameRejectedAction(): Action {
    return {
        type: CreateGameActionTypes.Rejected,
    };
}

export function createGameFulfilledAction(
    payload: IGame,
): IActionWithPayload<CreateGameActionTypes.Fulfilled, IGame> {
    return {
        type: CreateGameActionTypes.Fulfilled,
        payload: payload,
    };
}
