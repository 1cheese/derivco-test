import { route } from '../../../CorePackage/RouteModule/common/route';
import { AbstractRoute } from '../../../CorePackage/RouteModule/common/AbstractRoute';

@route()
export class CreateGameRoute extends AbstractRoute {
    public static url() {
        return '/games/create';
    }
}
