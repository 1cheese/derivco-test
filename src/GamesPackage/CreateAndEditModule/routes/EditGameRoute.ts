import { route } from '../../../CorePackage/RouteModule/common/route';
import { AbstractRoute } from '../../../CorePackage/RouteModule/common/AbstractRoute';
import { IGame } from '../../MainModule/interfaces/IGame';

type EditGameRouteParams = {
    id: IGame['id'];
};

@route<EditGameRouteParams>()
export class EditGameRoute extends AbstractRoute<EditGameRouteParams> {
    public static url({ id }: EditGameRouteParams = { id: ':id(\\w+)' }) {
        return `/games/edit/${id}`;
    }
}
