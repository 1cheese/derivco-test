import { call, put, takeLatest } from 'redux-saga/effects';

import { GamesEndpoint } from '../../../CorePackage/ApiModule/endpoints/GamesEndpoint';
import { IGame } from '../../MainModule/interfaces/IGame';
import { IActionWithPayload } from '../../../CorePackage/MainModule/interfaces/IActionWithPayload';
import { FetchGameByIdActionTypes } from '../enums/FetchGameByIdActionTypes';
import {
    fetchGameByIdFulfilledAction,
    fetchGameByIdPendingAction,
    fetchGameByIdRejectedAction,
} from '../actions/FetchGameByIdAction';

export function* fetchGameByIdSaga(
    action: IActionWithPayload<FetchGameByIdActionTypes.Initial, IGame['id']>,
) {
    try {
        yield put(fetchGameByIdPendingAction());
        const game: IGame = yield call(
            GamesEndpoint.getGameById,
            action.payload,
        );
        yield put(fetchGameByIdFulfilledAction(game));
    } catch (error) {
        yield put(fetchGameByIdRejectedAction());
    }
}

export function* watchFetchGameByIdSaga() {
    yield takeLatest(FetchGameByIdActionTypes.Initial, fetchGameByIdSaga);
}
