import { nanoid } from 'nanoid';
import { call, put, select, takeLatest } from 'redux-saga/effects';

import { GamesEndpoint } from '../../../CorePackage/ApiModule/endpoints/GamesEndpoint';
import { IGame } from '../../MainModule/interfaces/IGame';
import { IRootState } from '../../../CorePackage/MainModule/interfaces/IRootState';
import { createGameDataToSend } from '../utils/createGameDataToSend';
import {
    createGameFulfilledAction,
    createGamePendingAction,
    createGameRejectedAction,
} from '../actions/CreateGameAction';
import { CreateGameActionTypes } from '../enums/CreateGameActionTypes';
import { clearCreateAndEditGameStateAction } from '../actions/ClearCreateAndEditGameStateAction';
import { browserHistory } from '../../../CorePackage/MainModule/utils/browserHistory';
import { GamesListRoute } from '../../GamesListModule/routes/GamesListRoute';

export function* createGameSaga() {
    try {
        const state: IRootState = yield select();
        const gameId = nanoid(5);
        const createdGame = {
            id: gameId,
            ...createGameDataToSend(state),
        };
        yield put(createGamePendingAction());
        const game: IGame = yield call(GamesEndpoint.createGame, createdGame);
        yield put(createGameFulfilledAction(game));

        if (game.id !== undefined && game.id === gameId) {
            yield put(clearCreateAndEditGameStateAction());
            browserHistory.push(GamesListRoute.url());
        }
    } catch (error) {
        yield put(createGameRejectedAction());
    }
}

export function* watchCreateGameSaga() {
    yield takeLatest(CreateGameActionTypes.Initial, createGameSaga);
}
