import { call, put, takeLatest, select } from 'redux-saga/effects';

import { GamesEndpoint } from '../../../CorePackage/ApiModule/endpoints/GamesEndpoint';
import { IGame } from '../../MainModule/interfaces/IGame';
import { IActionWithPayload } from '../../../CorePackage/MainModule/interfaces/IActionWithPayload';
import { EditGameActionTypes } from '../enums/EditGameActionTypes';
import {
    editGameFulfilledAction,
    editGamePendingAction,
    editGameRejectedAction,
} from '../actions/EditGameAction';
import { IRootState } from '../../../CorePackage/MainModule/interfaces/IRootState';
import { createGameDataToSend } from '../utils/createGameDataToSend';
import { clearCreateAndEditGameStateAction } from '../actions/ClearCreateAndEditGameStateAction';
import { browserHistory } from '../../../CorePackage/MainModule/utils/browserHistory';
import { GamesListRoute } from '../../GamesListModule/routes/GamesListRoute';

export function* editGameSaga(
    action: IActionWithPayload<EditGameActionTypes.Initial, IGame['id']>,
) {
    try {
        const state: IRootState = yield select();
        const gameId = action.payload;
        const editedGame = {
            id: gameId,
            ...createGameDataToSend(state),
        };
        yield put(editGamePendingAction());
        const game: IGame = yield call(GamesEndpoint.editGame, editedGame);
        yield put(editGameFulfilledAction(game));

        if (game.id !== undefined && game.id === gameId) {
            yield put(clearCreateAndEditGameStateAction());
            browserHistory.push(GamesListRoute.url());
        }
    } catch (error) {
        yield put(editGameRejectedAction());
    }
}

export function* watchEditGameSaga() {
    yield takeLatest(EditGameActionTypes.Initial, editGameSaga);
}
