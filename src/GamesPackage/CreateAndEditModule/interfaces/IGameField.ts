import { GameFieldValue } from '../types/GameFieldValue';

export interface IGameField {
    value?: GameFieldValue;
    label?: string;
}
