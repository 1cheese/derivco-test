import { GameFieldAlias } from '../enums/GameFieldAlias';
import { GameFieldValue } from '../types/GameFieldValue';

export interface ISetGameFieldByAliasActionPayload {
    fieldAlias: GameFieldAlias;
    value: GameFieldValue;
}
