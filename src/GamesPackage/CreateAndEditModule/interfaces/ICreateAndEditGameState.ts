import { GameFieldAlias } from '../enums/GameFieldAlias';
import { IGameField } from './IGameField';

export interface ICreateAndEditGameState {
    isFetching: boolean;
    fields: Record<GameFieldAlias, IGameField>;
}
