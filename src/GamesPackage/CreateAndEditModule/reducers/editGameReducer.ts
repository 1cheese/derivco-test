import { Reducer } from 'redux';

import { createAndEditGameDefaultState } from '../utils/createAndEditGameDefaultState';
import { ICreateAndEditGameState } from '../interfaces/ICreateAndEditGameState';
import { IActionWithPayload } from '../../../CorePackage/MainModule/interfaces/IActionWithPayload';
import { EditGameActionTypes } from '../enums/EditGameActionTypes';
import { IGame } from '../../MainModule/interfaces/IGame';

type TAction = IActionWithPayload<EditGameActionTypes, IGame>;

export const editGameReducer: Reducer<ICreateAndEditGameState, TAction> = (
    state = createAndEditGameDefaultState,
    action,
): ICreateAndEditGameState => {
    switch (action.type) {
        case EditGameActionTypes.Pending:
            return {
                ...state,
                isFetching: true,
            };

        case EditGameActionTypes.Rejected:
        case EditGameActionTypes.Fulfilled:
            return {
                ...state,
                isFetching: false,
            };

        default:
            return state;
    }
};
