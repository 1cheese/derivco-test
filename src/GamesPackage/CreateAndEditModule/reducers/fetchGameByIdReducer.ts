import { Reducer } from 'redux';

import { ICreateAndEditGameState } from '../interfaces/ICreateAndEditGameState';
import { createAndEditGameDefaultState } from '../utils/createAndEditGameDefaultState';
import { FetchGameByIdActionTypes } from '../enums/FetchGameByIdActionTypes';
import { IGame } from '../../MainModule/interfaces/IGame';
import { IActionWithPayload } from '../../../CorePackage/MainModule/interfaces/IActionWithPayload';

type TAction = IActionWithPayload<FetchGameByIdActionTypes, IGame>;

export const fetchGameByIdReducer: Reducer<ICreateAndEditGameState, TAction> = (
    state = createAndEditGameDefaultState,
    action,
): ICreateAndEditGameState => {
    switch (action.type) {
        case FetchGameByIdActionTypes.Pending:
            return {
                ...state,
                isFetching: true,
            };

        case FetchGameByIdActionTypes.Rejected:
            return {
                ...state,
                isFetching: false,
            };

        case FetchGameByIdActionTypes.Fulfilled: {
            const newFieldsArray = Object.entries(state.fields).map(
                ([fieldAlias, fieldData]) => {
                    const key = fieldAlias as keyof IGame;

                    return [
                        fieldAlias,
                        {
                            ...fieldData,
                            value:
                                action.payload[key] === undefined
                                    ? fieldData.value
                                    : action.payload[key],
                        },
                    ];
                },
            );

            return {
                ...state,
                isFetching: false,
                fields: Object.fromEntries(newFieldsArray),
            };
        }

        default:
            return state;
    }
};
