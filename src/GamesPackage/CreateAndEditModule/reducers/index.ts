import reduceReducers from 'reduce-reducers';

import { createGameReducer } from './createGameReducer';
import { ICreateAndEditGameState } from '../interfaces/ICreateAndEditGameState';
import { createAndEditGameDefaultState } from '../utils/createAndEditGameDefaultState';
import { fetchGameByIdReducer } from './fetchGameByIdReducer';
import { gameBasicReducer } from './gameBasicReducer';
import { editGameReducer } from './editGameReducer';

export const createAndEditGameReducer = reduceReducers<
    ICreateAndEditGameState,
    any
>(
    createAndEditGameDefaultState ?? null,
    createGameReducer,
    fetchGameByIdReducer,
    gameBasicReducer,
    editGameReducer,
);
