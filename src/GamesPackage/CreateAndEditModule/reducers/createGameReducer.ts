import { Reducer } from 'redux';

import { ICreateAndEditGameState } from '../interfaces/ICreateAndEditGameState';
import { createAndEditGameDefaultState } from '../utils/createAndEditGameDefaultState';
import { IActionWithPayload } from '../../../CorePackage/MainModule/interfaces/IActionWithPayload';
import { CreateGameActionTypes } from '../enums/CreateGameActionTypes';
import { IGame } from '../../MainModule/interfaces/IGame';

type TAction = IActionWithPayload<CreateGameActionTypes, IGame>;

export const createGameReducer: Reducer<ICreateAndEditGameState, TAction> = (
    state = createAndEditGameDefaultState,
    action,
): ICreateAndEditGameState => {
    switch (action.type) {
        case CreateGameActionTypes.Pending:
            return {
                ...state,
                isFetching: true,
            };

        case CreateGameActionTypes.Fulfilled:
        case CreateGameActionTypes.Rejected:
            return {
                ...state,
                isFetching: false,
            };

        default:
            return state;
    }
};
