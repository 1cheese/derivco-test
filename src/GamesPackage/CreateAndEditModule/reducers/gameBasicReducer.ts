import { Action, Reducer } from 'redux';

import { ICreateAndEditGameState } from '../interfaces/ICreateAndEditGameState';
import { createAndEditGameDefaultState } from '../utils/createAndEditGameDefaultState';
import { IActionWithPayload } from '../../../CorePackage/MainModule/interfaces/IActionWithPayload';
import { SetGameFieldByAliasActionTypes } from '../enums/SetGameFieldByAliasActionTypes';
import { ClearCreateAndEditGameStateActionTypes } from '../enums/ClearCreateAndEditGameStateActionTypes';
import { ISetGameFieldByAliasActionPayload } from '../interfaces/ISetGameFieldByAliasPayload';

type TAction =
    | IActionWithPayload<
          SetGameFieldByAliasActionTypes,
          ISetGameFieldByAliasActionPayload
      >
    | Action<ClearCreateAndEditGameStateActionTypes>;

export const gameBasicReducer: Reducer<ICreateAndEditGameState, TAction> = (
    state = createAndEditGameDefaultState,
    action,
): ICreateAndEditGameState => {
    switch (action.type) {
        case SetGameFieldByAliasActionTypes.SetField:
            return {
                ...state,
                fields: {
                    ...state.fields,
                    [action.payload.fieldAlias]: {
                        ...state.fields[action.payload.fieldAlias],
                        value: action.payload.value,
                    },
                },
            };

        case ClearCreateAndEditGameStateActionTypes.ClearState: {
            return createAndEditGameDefaultState;
        }

        default:
            return state;
    }
};
