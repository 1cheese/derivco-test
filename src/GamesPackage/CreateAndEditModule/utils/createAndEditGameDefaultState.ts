import { ICreateAndEditGameState } from '../interfaces/ICreateAndEditGameState';
import { GameFieldAlias } from '../enums/GameFieldAlias';

export const createAndEditGameDefaultState: ICreateAndEditGameState = {
    isFetching: false,
    fields: {
        [GameFieldAlias.Name]: {},
        [GameFieldAlias.Description]: {
            label: 'Description',
        },
    },
};
