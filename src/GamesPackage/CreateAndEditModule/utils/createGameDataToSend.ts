import { IRootState } from '../../../CorePackage/MainModule/interfaces/IRootState';
import { selectGameFieldByFieldAlias } from '../selectors/selectGameFieldByFieldAlias';
import { GameFieldAlias } from '../enums/GameFieldAlias';
import { IGame } from '../../MainModule/interfaces/IGame';

export function createGameDataToSend(state: IRootState): Omit<IGame, 'id'> {
    const gameName = selectGameFieldByFieldAlias(GameFieldAlias.Name)(
        state,
    ).value;
    const gameDescription = selectGameFieldByFieldAlias(
        GameFieldAlias.Description,
    )(state).value;

    if (gameName == null || gameDescription == null) {
        throw new Error('Not all fields are filled');
    }

    return {
        name: gameName,
        description: gameDescription,
    };
}
