import * as React from 'react';
import { Route, RouteProps } from 'react-router-dom';

import { EditGameRoute } from '../routes/EditGameRoute';
import { CreateAndEditGamePage } from '../components/CreateAndEditGamePage';

type Props = RouteProps & {};

export class EditGameRouter extends React.Component<Props> {
    public static readonly defaultProps: Partial<Props> = {
        exact: true,
        path: EditGameRoute.url(),
    };

    public render() {
        return <Route {...this.props} component={CreateAndEditGamePage} />;
    }
}
