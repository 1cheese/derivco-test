import * as React from 'react';
import { Route, RouteProps } from 'react-router-dom';

import { CreateGameRoute } from '../routes/CreateGameRoute';
import { CreateAndEditGamePage } from '../components/CreateAndEditGamePage';

type Props = RouteProps & {};

export class CreateGameRouter extends React.Component<Props> {
    public static readonly defaultProps: Partial<Props> = {
        exact: true,
        path: CreateGameRoute.url(),
    };

    public render() {
        return <Route {...this.props} component={CreateAndEditGamePage} />;
    }
}
