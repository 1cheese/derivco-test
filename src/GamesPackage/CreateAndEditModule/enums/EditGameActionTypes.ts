export enum EditGameActionTypes {
    Initial = 'EDIT_GAME',
    Pending = 'EDIT_GAME_PENDING',
    Rejected = 'EDIT_GAME_REJECTED',
    Fulfilled = 'EDIT_GAME_FULFILLED',
}
