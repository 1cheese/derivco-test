export enum FetchGameByIdActionTypes {
    Initial = 'FETCH_GAME_BY_ID',
    Pending = 'FETCH_GAME_BY_ID_PENDING',
    Rejected = 'FETCH_GAME_BY_ID_REJECTED',
    Fulfilled = 'FETCH_GAME_BY_ID_FULFILLED',
}
