export enum CreateGameActionTypes {
    Initial = 'CREATE_GAME',
    Pending = 'CREATE_GAME_PENDING',
    Rejected = 'CREATE_GAME_REJECTED',
    Fulfilled = 'CREATE_GAME_FULFILLED',
}
