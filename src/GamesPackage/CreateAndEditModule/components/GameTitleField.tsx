import * as React from 'react';

import css from './GameTitleField.pcss';
import { TextField } from '../../../CorePackage/ControlsModule/components/TextField';
import { useDispatch, useSelector } from 'react-redux';
import { IRootState } from '../../../CorePackage/MainModule/interfaces/IRootState';
import { IGameField } from '../interfaces/IGameField';
import { selectGameFieldByFieldAlias } from '../selectors/selectGameFieldByFieldAlias';
import { GameFieldAlias } from '../enums/GameFieldAlias';
import { setGameFieldByAliasAction } from '../actions/SetGameFieldByAliasAction';
import { selectCreateAndEditGameIsFetching } from '../selectors/selectCreateAndEditGameIsFetching';

const GameTitleField: React.FC = () => {
    const fieldProps = useSelector<IRootState, IGameField>(state =>
        selectGameFieldByFieldAlias(GameFieldAlias.Name)(state),
    );
    const dispatch = useDispatch();
    const isFetching = useSelector(selectCreateAndEditGameIsFetching);

    const handleFieldChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        dispatch(
            setGameFieldByAliasAction({
                value: event.target.value,
                fieldAlias: GameFieldAlias.Name,
            }),
        );
    };

    return (
        <TextField
            {...fieldProps}
            disabled={isFetching}
            onChange={handleFieldChange}
            label={'Game name'}
            className={css.input}
        />
    );
};

export { GameTitleField };
