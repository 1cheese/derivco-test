import * as React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { RouteComponentProps } from 'react-router-dom';

import css from './CreateAndEditGamePage.pcss';
import { GameTitleField } from './GameTitleField';
import { PageTitle } from '../../../CorePackage/MainModule/components/PageTitle';
import { clearCreateAndEditGameStateAction } from '../actions/ClearCreateAndEditGameStateAction';
import { GameDescriptionField } from './GameDescriptionField';
import { fetchGameByIdAction } from '../actions/FetchGameByIdAction';
import { browserHistory } from '../../../CorePackage/MainModule/utils/browserHistory';
import { GamesListRoute } from '../../GamesListModule/routes/GamesListRoute';
import { Button } from '../../../CorePackage/ControlsModule/components/Button';
import { ButtonType } from '../../../CorePackage/ControlsModule/enums/ButtonType';
import { editGameAction } from '../actions/EditGameAction';
import { createGameAction } from '../actions/CreateGameAction';
import { selectCreateAndEditGameIsFetching } from '../selectors/selectCreateAndEditGameIsFetching';

const CreateAndEditGamePage: React.FC<
    RouteComponentProps<{
        id?: string;
    }>
> = props => {
    const dispatch = useDispatch();
    const isFetching = useSelector(selectCreateAndEditGameIsFetching);
    const gameId = props.match.params.id;
    const isEditPage = gameId !== undefined;

    React.useEffect(() => {
        if (isEditPage === true) {
            dispatch(fetchGameByIdAction(gameId as string));
        }
    }, [gameId]);

    const handleSaveButtonClick = () => {
        if (isEditPage === true) {
            dispatch(editGameAction(gameId as string));
            return;
        }

        dispatch(createGameAction());
    };

    const handleBackToGamesTransition = () => {
        dispatch(clearCreateAndEditGameStateAction());
        browserHistory.push(GamesListRoute.url());
    };

    return (
        <>
            <div className={css.back} onClick={handleBackToGamesTransition}>
                Back to Games
            </div>
            <PageTitle
                label={<GameTitleField />}
                buttons={[
                    <Button
                        buttonType={ButtonType.Success}
                        onClick={handleSaveButtonClick}
                        disabled={isFetching}
                    >
                        Save
                    </Button>,
                    <Button
                        onClick={handleBackToGamesTransition}
                        disabled={isFetching}
                    >
                        Cancel
                    </Button>,
                ]}
            />
            <div className={css.main}>
                <div className={css.field}>
                    <GameDescriptionField />
                </div>
            </div>
        </>
    );
};

export { CreateAndEditGamePage };
