import * as React from 'react';

import css from './GameDescriptionField.pcss';
import { TextField } from '../../../CorePackage/ControlsModule/components/TextField';
import { useDispatch, useSelector } from 'react-redux';
import { IRootState } from '../../../CorePackage/MainModule/interfaces/IRootState';
import { IGameField } from '../interfaces/IGameField';
import { selectGameFieldByFieldAlias } from '../selectors/selectGameFieldByFieldAlias';
import { GameFieldAlias } from '../enums/GameFieldAlias';
import { setGameFieldByAliasAction } from '../actions/SetGameFieldByAliasAction';
import { selectCreateAndEditGameIsFetching } from '../selectors/selectCreateAndEditGameIsFetching';

const GameDescriptionField: React.FC = () => {
    const fieldProps = useSelector<IRootState, IGameField>(state =>
        selectGameFieldByFieldAlias(GameFieldAlias.Description)(state),
    );
    const dispatch = useDispatch();
    const isFetching = useSelector(selectCreateAndEditGameIsFetching);

    const handleFieldChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        dispatch(
            setGameFieldByAliasAction({
                value: event.target.value,
                fieldAlias: GameFieldAlias.Description,
            }),
        );
    };

    return (
        <TextField
            {...fieldProps}
            className={css.component}
            textarea={true}
            disabled={isFetching}
            onChange={handleFieldChange}
        />
    );
};

export { GameDescriptionField };
