# Derivco Test

## To start project locally run
`npm start`

## Working demo can also be found here
[https://derivco-test-zverev.netlify.app/](https://derivco-test-zverev.netlify.app/)

#### P.S. Game cards ordering might be done via Drag'n'Drop functionality
