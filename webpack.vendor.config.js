const path = require('path');
const webpack = require('webpack');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');

const pkg = require('./package');
const config = require('./build.config');

module.exports = () => ({
    entry: {
        vendor: Object.keys(pkg.dependencies),
    },

    output: {
        path: path.resolve(__dirname, config.distDirectory),
        filename: '[name].[fullhash].dll.js',
        library: 'vendor',
    },

    plugins: [
        new CleanWebpackPlugin(),
        new CopyPlugin({
            patterns: [{ from: 'assets', to: 'assets' }],
        }),
        new webpack.DllPlugin({
            name: 'vendor',
            path: path.resolve(
                __dirname,
                config.distDirectory,
                'manifest.json',
            ),
        }),
    ],

    module: {
        rules: [
            {
                test: /\.css$/,
                use: [
                    'style-loader',
                    {
                        loader: 'css-loader',
                        options: {
                            modules: {
                                auto: true,
                                localIdentName: '[local]',
                            },
                        },
                    },
                ],
            },
        ],
    },
});
