const path = require('path');
const webpack = require('webpack');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const AddAssetHtmlWebpackPlugin = require('add-asset-html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const autoprefixer = require('autoprefixer');
const postcssUnits = require('postcss-units');
const postcssNested = require('postcss-nested');

const pkg = require('./package');
const config = require('./build.config');
const serveConfig = require('./serve.config');

module.exports = (env, argv) => {
    const isProduction = argv.mode === 'production';

    return {
        devtool: isProduction ? 'nosources-source-map' : 'eval-source-map',

        entry: {
            main: './src/index.ts',
        },

        output: {
            path: path.resolve(__dirname, config.distDirectory),
            filename: 'bundle.[fullhash].js',
            publicPath: '/',
        },

        resolve: {
            extensions: ['.ts', '.tsx', '.js'],
            symlinks: false,
        },

        module: {
            rules: [
                {
                    test: /\.tsx?$/,
                    loader: 'ts-loader',
                },
                {
                    test: /\.pcss$/,
                    use: [
                        {
                            loader: isProduction
                                ? MiniCssExtractPlugin.loader
                                : 'style-loader',
                        },
                        {
                            loader: 'css-loader',
                            options: {
                                modules: {
                                    localIdentName: isProduction
                                        ? '[hash:base64]'
                                        : '[name]__[local]___[hash:base64:5]',
                                },
                            },
                        },
                        {
                            loader: 'postcss-loader',
                            options: {
                                sourceMap: isProduction === false,
                                postcssOptions: {
                                    plugins: [
                                        autoprefixer,
                                        postcssUnits({
                                            precision: 4,
                                        }),
                                        postcssNested(),
                                    ],
                                },
                            },
                        },
                    ],
                },
                {
                    test: /\.css$/,
                    use: [
                        {
                            loader: 'style-loader',
                        },
                        {
                            loader: 'css-loader',
                            options: {
                                modules: false,
                            },
                        },
                    ],
                    include: [path.resolve(__dirname, 'node_modules')],
                },
                {
                    test: /\.(png|jpg|svg|webp)$/,
                    use: [
                        {
                            loader: 'url-loader',
                            options: {
                                limit: 8192,
                            },
                        },
                    ],
                },
                {
                    test: /\.(woff2?)$/,
                    use: [
                        {
                            loader: 'file-loader',
                        },
                    ],
                },
            ],
        },

        plugins: [
            new CleanWebpackPlugin({
                cleanOnceBeforeBuildPatterns: [
                    `${config.distDirectory}/bundle.*.js`,
                    `${config.distDirectory}/*.html`,
                ],
            }),
            new webpack.DllReferencePlugin({
                context: __dirname,
                manifest: path.resolve(
                    __dirname,
                    config.distDirectory,
                    'manifest.json',
                ),
            }),
            new webpack.DefinePlugin({
                'process.env': {
                    BASE_PATH: JSON.stringify(config.getApiUrl(isProduction)),
                },
            }),
            ...(isProduction
                ? [
                      new MiniCssExtractPlugin({
                          filename: '[name].[hash].css',
                          chunkFilename: '[id].hash.css',
                      }),
                  ]
                : []),
            new HtmlWebpackPlugin({
                template: 'src/index.html',
                filename: 'index.html',
                favicon: 'assets/img/favicon.png',
                title: 'Derivco Test',
                meta: {
                    'application-name': pkg.name,
                    'version': pkg.version,
                    'charset': 'UTF-8',
                    'viewport': 'width=device-width, initial-scale=1.0',
                },
            }),
            new AddAssetHtmlWebpackPlugin({
                filepath: path.resolve(
                    __dirname,
                    config.distDirectory,
                    '*.dll.js',
                ),
                hash: true,
            }),
        ],

        devServer: serveConfig,
    };
};
