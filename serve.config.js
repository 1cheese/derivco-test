const path = require('path');
const jsonServer = require('json-server');
const webpackDevServerWaitPage = require('webpack-dev-server-waitpage');
const cors = require('cors');

const pkg = require('./package');
const config = require('./build.config');

function requireWithoutCache(module) {
    delete require.cache[require.resolve(module)];

    return require(module);
}

const apiProxyOptions = {
    target: config.getApiUrl(false),
    secure: false,
    changeOrigin: false,
    ws: true,
    pathRewrite: {
        [`^${config.serviceRoot}/api`]: '/api',
    },
    router: () => requireWithoutCache('./build.config').getApiUrl(false),
};

const waitPageOptions = {
    title: pkg.name,
    theme: 'material',
};

module.exports = {
    open: true,
    hot: true,
    static: {
        directory: path.join(__dirname, config.distDirectory),
    },
    devMiddleware: {
        publicPath: '/',
    },
    client: {
        logging: 'info',
        overlay: true,
    },
    onBeforeSetupMiddleware: devServer => {
        const serve = jsonServer.create();
        const router = jsonServer.router(config.jsonServer.path);

        serve.use(cors());
        serve.use(
            jsonServer.rewriter({
                '/api/*': '/$1',
            }),
        );
        serve.use(function (req, res, next) {
            const timeout = Math.floor(Math.random() * 1000) + 500;
            setTimeout(next, timeout);
        });
        serve.use(router);
        serve.listen(config.jsonServer.port, () => {
            console.log(
                `JSON Server is running at ${config.jsonServer.port} port`,
            );
        });

        devServer.app.use(webpackDevServerWaitPage(devServer, waitPageOptions));
    },
    proxy: {
        '/api': apiProxyOptions,
    },
    port: 9119,
    historyApiFallback: true,
};
